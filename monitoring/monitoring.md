# Monitoring opensource

## objectifs

L'objectifs est d'outiller le processus de gestion des évènements afin de garantir un niveau de service.

Le monitoring permet simplement de détecter les indisponibilité et les incidents afin d'informer les différents intervenant de la situation afin de réduire l'impact de ceux-ci.

Enfin les outils de monitoring permettent de mesurer la qualité de service.

## Principe

Le système de monitoring dispose d'un modèle simplifié de l'infrastructure de service dans sa configuration : un ensemble de **ressources** sur un ensemble de **host**. Pour chacun des hosts et chacunes des ressources des **sondes** définissent une méthode de **mesure** et des **seuils** de tolérence. Ainsi il est possible de définir des règles de surveillance de l'infrastructure. le système de monitoring déclenche une **Alerte** lorsqu'une règle est transgressée.

Exemple : la taux d'occupation d'un filesystème en espace de stockage et en inode doit être inférieur à 80% de l'espace total. Au dela l'équipe en charge sera informée

Deux méthode de monitoring sont utilisé et coexistent dans la plupârt des solution de monitoring:

- la surveillance active : l'outil de surveillance viens mesurer régulièrement l'état d'une ressource.
  - avantages : Les mesure régulière permettent de disposer de graphique et le systeme detecte les absences et indisponibilités des hosts surveillé
  - inconvénients : le coût en terme de ressources pour effectuer la surveillance et maintenir le modèle de l'infrastructure.
- La surveillance passive : les systèmes remontent des évènements vers le système de surveillance.
  - avantages : les anomalies ponctuelles sont systèmatiquement visible.
  - inconvénient : un incident lié à la joignabilité des ressources n'est pas visible.

Ces deux méthodes permettent aussi la collecte des mettriques indispenssable à la gestion des capacité.

> Il deviens de plus en plus complexe de gérer les capacités des ressources pour lesquels il est possible de faire de la sur-reservation (thin-provisionning stockage, virtualisation etc...).

### relation avec les processus MCO

__Gestion des évènements et des alertes :__ Un évènement qui nécessite une action humaine est une alerte. L'intervention humaine consiste en :

- la validation de l'alerte puis
- la déclaration d'un incident,
- l'alerte est alors "acknowledgée" (marquée comme prise en main)
Elle sera finalement close par le retour à la normal qui est parfois liè au traitement de l'incident. Une revue régulière des alarmes acknowledgés devra être mis en place afin de s'assurer que les incidents lié sont toujours en cours de traitement.

__Gestion des incidents :__ Consiste à remettre en état le service ou la ressource concerné :
  - Modification du seuil d'alarme (oui souvent il n'y a que ça à faire)
  - Rédémarer ou basculer le service
  - Libération de ressources disponible
  L'incident sera clos lorsque les alertes associées disparaissent.
  Le traitement incidents donne parfois lieu à la déclaration d'un probleme

__Gestion des problèmes :__ Consiste en l'analyse aprofondie d'anomalies afin d'en découvrir la source et déviter leur reproduction. Ce processus donne lieu à la mise en place de nouvelles sondes de monitoring et de procédures de résolution.

__Gestion des changements :__ Il conviens de désactiver les système de monitoring durant le déploiement d'un changement afin d'éviter le déclenchement d'alertes non souhaitées.

__Gestion des capacités :__ la revue des metriques de consomation de ressource permet d'anticiper la saturation de celle-ci. Des sondes de surveillance évoluées peuvent déclencher une alarme sur une croissance de consomation en plus d'un dépassement de seuil.

## Architecture de la solution de monitoring

Au niveau réseaux, la solution de monitoring dois accèder aux service via le même réseaux que les consomateurs du service supervisé d'une part, mais aussi au réseaux de management des système supervisé.

La supervision à un cout important en terme de ressources aussi au dela d'une certain volume de services il faudra concevoir une architecture distribuée.

Répartition des hosts sur plusieurs collecteur d'évènements et un système de centralisation de ceux-ci. A plusieur niveau de l'architecture un brocker d'évènement asynchrone permettra de fluidifier le traitement des gros volumes.

## Nagios

Nagios de part sa popularité est devenue un standard de monitoring si bien que plusieurs solutions reproduisent le même modèle fonctionnel ou est compatible avec ses différents plugins de monitoring.

Nagios à partir d'une configuration définissant des objets, ordonance des mesures actives, receptionne des mesures passives et alerte des contact.

### Les objets

Comme la plupart des solutions opensource Nagios propose un modèle fonctionnel dans lequel on pourra implémenter notre besoin.

- host : une entité du réseaux connue (un nom, une ip, une mesure d'existance souvant via un ping )
- service : une mesure de monitoring (un nom, un host, une commande qui mesure, un seuil warning, un seuil critique et un groupe de contact)
- contact : une entité qui sera alertée (un nom, une méthode d'alerte)

Il existe bien sur des groupes de hosts de services et de contact et de façon plus enecdotique des dépendances de services.

Chacune des méthodes d'alerte ou de mesure sont en fait une commande lancée par nagios. Pour chaque action nagios fork des processus et attends leur retour (mode synchrone).

### Les plugins nagios

Les checks actifs sont en fait des ligne de commande exécuté par nagios, ainsi tout ce qui peu être fait en ligne de commande peu être monitoré.

L'api est simple, la commande s'exécute et retourne une ligne de message et un code retour valant :

- 0 si la mesure est correcte,
- 1 si l'état de la mesure est inquiétante (warning)
- et enfin 2 si l'état est critique,

Tout autre code retour est vue comme un état inconnu (unknown).

cette API simplissime est devenue elle aussi un standard les plugins nagios sont repris par beaucoup de solution de monitoring.

### écoute d'évènements (mode passif)

Nagios utilise un fichier "pipe" une pile fifo nommé dans l'arborescence (commande mkfifo fichier), les messages sont dépillé très régulièrement par le daemon nagios.

Tout système de monitoring passif lié vien écrire un message dans ce fichier dans le format suivant :

```
[<timestamp>] PROCESS_SERVICE_CHECK_RESULT;<host_name>;<svc_name>;<return_code>;<plugin_output>
```

On utilise donc la même api : un message et un code retour associé à un service sur un host.

## Check_mk

Check_mk est composé de 3 éléments :

- un plugins nagios qui collecte une mesure
- un agent déployé sur les host à monitoré.
- un générateur de configuration nagios

Dans le cadre d'un déploiement de monitoring, **le générateur** de config viens appeler **l'agent** distants et récupère toutes les métriques que l'agent arrive à collecter. suivant des règles le générateur défini un service actif check_mk et des services passif sur le host nagios.

Ainsi Nagios exécute alors un seul check actif sur le host qui collecte l'ensemble des messures et les transmet en check passif via le fichier fifo à nagios.

Check_mk apporte alors à la fois une solution de simplification de la configuration Nagios mais aussi permettant de réduire la charge du monitoring en réduisant grandement la charge de monitoring.

L'agent est un simple script python qui collecte les mesures et les retourne sur sa sortie standard. Lorsqu'on l'installe on l'associé avec le daemon Unix inetd (xinetd sous linux voir même systemd) qui écoute sur le port TCP 6556, lance le script et retourne sa sortie standard sur chaque connexion entrante.

Bien sur il conviendra de ne par laisser ce port ouvert à toutes connexion inetd contien un filtre qu'il suffit d'activer.
