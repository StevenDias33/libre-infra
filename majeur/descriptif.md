# Informatique libre dans les infrastructures de services

Le libre est une philosophie ouverte qui permet d'avoir une infrastructure hybride ou complètement auto-hébergée, et ce, à coût maîtrisé.

Le support et l'expérience de la communauté, ainsi que le respect des standards, permettent de garantir l'autonomie du SI dans le temps.

## Présentation

Les outils libres et opensource occupent désormais de fait une place prépondérante dans le monde de l'IT. En effet, avec le temps, le caractère libre des outils et logiciels est devenu un gage de qualité. Cette qualité des outils libres se manifeste à travers plusieurs aspects, parmi lesquels : la **sécurité** des outils et des processus, la **maturité** des solutions technologiques et la complète **transparence** sur le fonctionnement interne.
Cette qualité accrue a permis notamment aux systèmes GNU/Linux de s'imposer de façon drastique chez les techniciens, en particulier dans les infrastructures.

A l'heure où IBM rachête RedHat (3ieme plus gros rachat dans l'histoir de la tech) pour se sauver, où une petite société comme Docker a su se faire une place de choix au sein des écosystèmes Cloud ; mais aussi à l'heure où Microsoft et Google sont des acteurs majeurs du libres et de l'opensource, il est bon de s'intéresser à cette façon de créer et d'innover.

Cette majeure a donc pour objectif l'appréhension approfondie des infrastructures basées sur des outils libres, ainsi que le développement d'une expertise technique *via* l'étude théorique de certains procédés techniques et la mise en pratique de certaines technologies.  

L'accent sera mis sur le **libre** (et non "l'opensource").

## Pré-Requis

* Savoir travailler en environnement GNU/Linux et POSIX
* Avoir des bases d'administration system sous GNU/linux
* Savoir utiliser git

## Objectifs

* Comprendre les bénéfices concrets liés à l'application de la philosophie du libre et quelles sont les contraintes associées.
* Approfondir les connaissances sur le fonctionnement des systèmes GNU/Linux
* Apprehender certaines briques libres et open sources couraments utilisée
* Avoir une vision sur l'écosystème de l'informatique libre
  * cloud
  * déploiement
  * stockage
  * réseau
  * sauvegarde
  * monitoring
  * etc.
* Savoir mettre en oeuvre une solution technique pérènne basée sur des outils libres
  * couche OS/kernel
  * gestion d'OS et de services
  * maîtrise du packaging (de solutions comme d'infrastructures)
  * conteneurisation & clustering
  * déploiement cloud
  * gestion et déploiement de configuration
  * automatisation et infrastructure-as-code
* Savoir contribuer à l'écosystem en :
  * contribuant à un projet existant
  * apportant une solution utile à la communauté

## Notation

### TP

Les TP sont notés en tenant compte des participants de façon individuelle, en se basant sur leurs niveaux et leurs attentes (qui sont différents).
Une grille est définie pour chaque TP ce qui permet d'avoir une vision simple sur qui a fait quoi.

### Questionaires

De façon récurente des petits questionaires permettent d'évaluer le niveau de connaissance.

