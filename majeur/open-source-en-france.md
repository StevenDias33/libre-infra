# Les projets Open source

## Chronique d'un projet libre ou open-source

### Naissance

* Un hack, une expérience, une recherche, tentative humble de réaliser quelques chose. (linux)
* Un besoin née d'une réflexion et/ou de l'absence de solution libre ou ouverte (nagios/netbox)
* Livraison d'un projet à la communautée (probablement associé à la création d'une fondation)
* ajout sur un projet existant

### Gains pour la comunautés

* Une solution disponible à moindre coût
* Nouvel Apport fonctionnel ouvert : implémentation métié adaptable offrant un framework (nagios, docker)
* Base de recherche et source d'inspiration.

### Apport de la communuté

* Utilisation/Evaluation/tests
  * retours d'utilisation et propositions
  * déclaration d'anomalie
* Implication dans le projet :
  * mediatisation : publication externe,
  * relecture/traduction de doc
* participation au projet :
  * participation active dans le user group
  * proposer une pull request de code
* gestion d'un projet connexe
  * plugins / add on
  * communauté d'utilisateur et partage de ressources

### L'essort

* Le projet trouve ses clients et utilisateurs
* Une structure s'associe au projet et stabilise son financement.

#### Le Financement

* Dons des utilisateurs
* Sponsort de Fondations : Objectif de la fondation
* Mecenat d'Entreprises : Quid de la contrepartie
* Vente de services/solution associés : L'Autonomie ou la galère

### Le déclin ?

* Bascule du coté fermé.
* Les Forks à destination rentable.
* L'Obsolescence lié à l'émergence d'une nouvelle tendence (ou Rachat par IBM...)

## Les fondations

Voir l'[histoire d'Unix](https://fr.wikipedia.org/wiki/Unix)

### Free software fondation FSF

créé par Monsieur Richard Stallman pour financer le projet GNU et promouvoir le logiciel libre

La FSF est une organisation américaine à but non lucratif fondée par Richard Stallman le 4 octobre 1985, dont la mission est la promotion du logiciel libre et la défense des utilisateurs. La FSF aide également au financement du projet GNU depuis l'origine. Son nom est associé au mouvement du logiciel libre. ([wikipédia](https://fr.wikipedia.org/wiki/Free_Software_Foundation))

#### Les projets de la FSF

Le projet GNU a une dimension social ethique et politique : L'objectif est d'avoir une version **libre** des logiciels courament utilisés

* GNU
  * GNU/Linux et GNU/hurd
  * Debian
  * GNOME
* system exploitation pour téléphone : <https://replicant.us/>
* manipulation d'images : The Gimp
* video : Blender
* Collection SAASS Service As A Software Substitute : <https://directory.fsf.org/wiki/Collection:SaaSS>
* format de fichier : <https://directory.fsf.org/wiki/Collection:File_format>

> [projets prioritaires de la FSF](https://www.fsf.org/campaigns/priority-projects/)

### L'open group

Issue de la fusion entre :

* l'**Open Software Foundation**, organisation fondée en 1988 (Apollo, Bull, DEC, HP, IBM, Nixdorf) pour créé un standard ouvert pour le système d'exploitation Unix ([wikipedia](https://fr.wikipedia.org/wiki/Open_Software_Foundation))
* et l' **X/Open** consortium européen (Bull, ICL, Siemens, Olivetti et Nixdorf) voulant definir une spécification commune aux systèmes d'exploitation dérivés d'UNIX

#### Les projet de l'open group

* Single Unix specification et UNIX98
* POSIX
* CDE (common Desktop environrment)
* LDAP
* Linux Standard Base
* X.org

## Entreprenariat

### Contexte européen

Pour l'europe, la filière open source est au coeur d’enjeux majeurs en termes économiques, de souveraineté, de sécurité et d’éthique.

Des recherche (Fraunhofer Institute / Open Forum Europe / European Commission, 2021) montrent que le logiciel libre représente, à l’échelle de l’Union européenne :

* Un impact économique estimé entre 65 et 95 milliards d'euros
* Un milliard d'euros d'investisement publique ou privé
* Une production de code de 16000 développeurs à temps plein, réutilisable par les secteurs privé et public
* Un potentiel d’augmentation du PIB de 100 milliards d'euros, et la créations de 1000 entreprises numériques par an, si les contributions au code open source s’accroissaient de 10%

la france est en 3ième position après l'allemagne et la grande bretagne en nombre de commit sur GitHub

### Enquête

D'aprés l'étude de la CNLL [État des lieux de la filière open source en France 2020/2021][1]

#### 134 entreprises

* La plupart sont des des microentreprises (59%) et des PME (35,1%)
* Leur age médian est de 11 ans et demi.
* Répartie sur tout la france (mais 43% en ile de france)
* Dont les clients preque autant publique que privé

### Modele economique

* Plus de la moitiée edite au moins un logiciel
* Type de facturation utilisé par les entreprises :
  * 78% font des factures au temps passé
  * 72% au forfait
  * 50% à l'usage
  * 40% en abonnement
* 20% du chiffre en moyen est réalisé hors de france

### Un marché difficile

* Souvent obligé de vendre du services pour financer l'edition
* Certains clients ne jouent pas le jeux
  * Se contentent de la version OSE
  * Ne vois que le coté gratuit et pas l'investissement
  * Ne contribue pas sufisament
    * Reverser le code ajouté les corrections
    * Retour d'experience sur l'intégration réaliser

En bref un manque de partenariat

Le marcher reste prometeur et les clients sont attirés par les **garanties** qu'apporte l'open source.

* Interoperabilité et format ourvert
* Securité
* Disponibilité
* Scalabilité

## Conclusion

C'est principalement l'apport de garantie qui est attendu par les clients des entreprises de l'open sources. En parallèle c'est la facilité d'utilisation des fonctionnalités apporté qui fait le succés d'une solution open source auprès des utilisateur.

En conséquence c'est bien le **niveau de service** (utilité*garantie) qui doit être la priorité des solutions open sources.

[1]: <https://cnll.fr/media/etude-cnll-2021.pdf>
