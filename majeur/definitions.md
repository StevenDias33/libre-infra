# Definitions

## Plateforme de service

Ensemble cohérent d'hôtes, configuration et applicatifs produisant un ou plusieurs services informatique.

Exemple : pour un site web c'est les hôtes configuré pour supporter les serveur web et de base de données necessaire au fonctionnement du site.

## Les configurations d'une plateforme de service

C'est l'ensemble des éléments et objets déployé et modifié sur le groupe d'hôtes afin de déployer la platforme de service

Exemple : configuration de vm, system déployés, parametrage systeme de stockage, comptes, groupes, package, fichier de configuration, stucture de base de donnée etc...

## Le versionning des configurations

Ensemble de moyen mis en oeuvre afin de tracer et d'identifier toutes les modifications effectuées sur les configurations.

## Le contrôle de conformité des configurations

Activité consistant a confronté les configurations réel d'une platforme de service avec la version documenté.

## La révocation de configuration

Opération qui consiste à dé-provisioner une configuration : supprimer tout les éléments de configuration déployé par cette configuration jusqu'a remettre en état l'hôte supprotant cette configuration.

## L'idempotence

c'est la faculté qu'un processus à lorsqu'il produit le même résultat peu importe le nombre de fois ou il est exécuté.

Exemple : Le tri d'une liste peu s'appliquer plusieurs fois à cette liste. le resultat du tri restera la liste des élément trié.

## La gestion des configurations

Ensemble des techniques et de moyens permettant de garantir la conformité des configurations en production avec leur version documenté.

On garanti ainsi notre capacité à redéployer complètement une platforme de service.

## Le kernel

Le noyau de système d’exploitation est **LE** programme qui gère les ressources matérielle comme les disques, les écrans et le clavier mais surtout la mémoire et le temps processeur alloué aux autres programmes.

[wikipedia: noyau de système d'exploitation](https://fr.wikipedia.org/wiki/Noyau_de_syst%C3%A8me_d%27exploitation)

## Arborescence

C'est une méthode d'organisation hiérarchique de la donnée dans laquelle chaque élément :

* peut être le parent de plusieurs autres éléments;
* dispose au maximum d'un seul parent.
La **racine** est le point de départ de l'arborescence ; c'est aussi le seul élément qui n'a pas de parent.

[wikipedia:arborescence](https://fr.wikipedia.org/wiki/Arborescence)

## Bibliothèques partagées

Ce sont des morceaux de logiciel compilé et donc directement utilisables par plusieurs autres logiciels ; Une bibliothèque de fonction et d'algorythmes qui peuvent être appelés par d'autres programmes

Exemple:

* `zlib.so` est une bibliothèques de fonctions utilisant l'algorithme de compression décompression "deflate"
* `libacl.so` est une bibliothèque contenant des fonctions pour manipuler les droits POSIX (les droits `rwx` sur les fichiers principalement)

L'utilisation de bibliothèques partagées permet de ne pas reproduire et donc maintenir plusieurs fois la même fonctionalité

## Compilation

Opération qui consiste en la construction de logiciel (exécutable) à partir du code source (lisible)

[wikipedia:compilation](https://fr.wikipedia.org/wiki/Compilateur)

## regex (expressions régulières)

Courament utilisée en programmation une expression régulière est une chaine de caratères définissant exactement tout un ensemble de chaines de caratères

Exemple : `\.jpg$` représente toute les chaines de caratères qui finissent pars ".jpg"

[wikipedia: regex](https://fr.wikipedia.org/wiki/regex)  
[regex101 : un site sympa sur les regex](https://regex101.com/)

## syscall : appel systeme

Un appels système consiste en la sollicitation du kernel pour accèder aux ressources, lorsqu'un programme ouvre un fichier il le fait via un appel système.

[wikipedia: apels système](https://fr.wikipedia.org/wiki/Appel_syst%C3%A8me)

## swap

La swap est espace de stockage de donnée physique utilisé pour stocker temporairement de la données 'chaude' initialement prévue pour être stocké en mémoire vive. L'utilisation de la swap permet au kernel du système d'exploitation de libérer temporairement de la memoire vive afin d'optimiser son usage.

[wikipedia: swap](https://fr.wikipedia.org/wiki/Espace_d%27%C3%A9change)
