# Linux

[rappels](./rappels-linux)

## La conteneurisation

- [Les containers](./containers.md)

### TP conteneurisation

- [containeurs en profondeur](./tp-containers/ct-in-depth/README.md)
- [outils lié à la conteneurisation](./tp-containers/ct-tooling/README.md)
- [Soyez le plus rapide](./tp-containers/be-the-fastest/README.md)

## Advanced GNU/linux

- [systemd](./systemd.md)
- [tp systemd](./tp-systemd/README.md)
- [hardening du systeme](./linux_os_hardening.md)
- [tp hardening](./tp-hardening/README.md)

