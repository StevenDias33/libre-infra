# Hardening

# Sommaire

- [Hardening](#hardening)
- [Sommaire](#sommaire)
- [I. Syscalls](#i-syscalls)
  - [1. Exploration](#1-exploration)
  - [2. Logs](#2-logs)
    - [A. Intro auditd](#a-intro-auditd)
    - [B. Conf auditd](#b-conf-auditd)
  - [3. Intégration](#3-intégration)
    - [A. systemd](#a-systemd)
    - [B. Docker](#b-docker)
- [II. SELinux](#ii-selinux)
  - [1. Exploration](#1-exploration-1)
    - [A. Config](#a-config)
    - [B. The Z option](#b-the-z-option)
  - [2. Simple example](#2-simple-example)

# I. Syscalls

## 1. Exploration

La commande `strace` permet de suivre en temps réel les *syscalls* que passe un programme pendant son exécution.

Son utilisation est très simple :

```bash
# Afficher les syscalls effectués par un programme
$ strace <COMMAND>
$ strace -c <COMMAND>

# strace un processus existant, il faut connaître son PID
$ strace -p <PID>

# on peut enregistrer la sortie de strace plutôt que l'afficher en sortie standard
$ strace -o /path/to/file <COMMAND>
```

## 2. Logs

### A. Intro auditd

Pour obtenir des logs sur les *syscall* passés au sein de l'OS, de façon passive, on va utiliser le démon `auditd` présent par défaut sur les systèmes RedHat.

Vérifier qu'il est activé :

```bash
$ systemctl status auditd
$ sudo audictl -s
$ sudo audictl -l
$ auditctl --help
...
```

Les fichiers liés à `auditd` :

- la conf : `/etc/audit/`
- les logs générés :
  - consultables avec `ausearch`
  - stockés dans `/var/log/audit/audit.log` par défaut

### B. Conf auditd

On peut tester/ajouter de la conf depuis la ligne de commande avec `auditctl` :

```bash
# Surveille les appels au syscall execve (appelé à chaque commande exécutée)
$ sudo auditctl -a always,exit -S execve -k key=suspect_command -F arch=b64

$ sudo auditctl -l
```

Dans cette commande :

- `-a` indique d'ajouter la commande
  - on précise en argument à quelle chaîne l'ajouter, ici `always` et `exit` indiquent qu'on va logger dès qu'un appel système commence son exécution, dès qu'il est appelé
- `-S` indique l'appel système que l'on souhaite surveiller
- `-k` permet de tagger l'alerte
  - `key=suspect_command` est un tag arbitraire, nous permettant de gérer les logs plus facilement
  - `arch=b64` indique l'architecture ciblée

Pour gérer les règles *via* un fichier de conf, il est possible d'écrire les règles directement dans un fichier du dossier `/etc/audit/rules.d/`, avec le contenu suivant :

```bash
-a always,exit -S execve -k key=suspect_command -F arch=b64
```

> Ce sont simplement les commandes, sans préciser `auditctl` devant.

## 3. Intégration

### A. systemd

Dans une unité *systemd* il est possible d'utiliser la clause `SystemCallFilter=` afin de préciser une liste de *syscalls* qui sont autorisés.

Si un *syscall* non-autorisé est utilisé par le processus, il sera tué.

> Voir [la page de doc dédiée](https://www.freedesktop.org/software/systemd/man/systemd.exec.html#SystemCallFilter=).

### B. Docker

Il est possible d'associer une politique *seccomp* à chaque conteneur à son lancement, ou bien définir une politique *seccomp* globale utilisée par défaut à chaque lancement de conteneurs.

Sur des systèmes modernes, Docker fournit un profil *seccomp* par défaut, qui bloque un certains nombres d'appels système, mais reste surtout compatible avec beaucoup d'applications. Il est nécessaire de compléter ce profil *seccomp* avec des règles maison pour renforcer la sécurité des conteneurs Docker.

> Voir [la page de doc dédiée](https://docs.docker.com/engine/security/seccomp/)

# II. SELinux

## 1. Exploration

### A. Config

Commandes SELinux :

```bash
# Etat de SELinux
$ sestatus
$ getenforce
```

Le fichier de conf principal de SELinux se trouve dans `/etc/selinux/config`.

### B. The Z option

La plupart des commandes possèdent, dans leur version moderne, une option `-Z` qui va nous permettre de lister les attributs SELinux de différents objets du système.

```bash
$ ls -al -Z
$ ps -ef -Z
$ id -Z
...
```

On utilise ces options en permanence pour vérifier l'état du système.

## 2. Simple example

➜ **Assurez-vous que SELinux est activé en mode `enforcing`**

➜ **Installez Apache (paquet `httpd`)**

➜ **Conf Apache, vitefé**

- supprimez le fichier `/etc/httpd/conf.d/welcome.conf`
- créez un fichier `/var/www/html/index.html`
- démarrez Apache `sudo systemctl start httpd`
- vérifiez que vous pouvez requêter votre fichier `curl localhost`

➜ **SELinux ?**

- vérifier le label des processus Apache
- vérifier le label du fichier dans `/var/www/html`

➜ **Getting denied**

- créer un fichier `/srv/html/index.html` avec le contenu de votre choix
- vérifiez son label : il est différent
- modifier la conf Apache `/etc/httpd/conf/httpd.conf` pour qu'il utilise le dossier `/srv/html` au lieu de `/var/www/html`
- restart : `sudo systemctl restart httpd`
- `curl localhost` : *403 Forbidden*

Lorsqu'une règle SELinux est enfreinte, on appelle cet évènement un *avc* et il est loggé :

```bash
$ sudo cat /var/log/audit/audit.log | grep avc
$ sudo sealert -a /var/log/audit/audit.log
...
```

Il est possible de changer le label de `/srv/html/index.html` à la main avec la commande `chcon`.  
Mais étant donné qu'on sait que le répertoire `/var/www/html/` avait le bon label, on peut se contenter de demander à `chcon` de copier le label de `/var/www/html` vers `/srv/html` :

```bash
# vérification des labels avant opération
$ ls -alZ /srv/html
$ ls -alZ /var/www/html
# on copie les labels d'un dossier, vers un autre
$ sudo chcon --reference /var/www/html /srv/html
# vérification des labels après opération
$ ls -alZ /srv/html
```

> Il aurait été aussi possible de générer automatiquement une règle pour whitelister cette interaction, à partir des logs, avec `cat /var/log/audit/audit.log | audit2allow`. On préfère pour cette technique passer SELinux en mode `permissive` afin de générer tous les logs pendant le fonctionnement de l'application. Ceci permet de générer une politique exhaustive pour le fonctionnement normal de l'application.
