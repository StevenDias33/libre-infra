# Containers

Dans ce cours, on va aborder plusieurs aspects de la conteneurisation :

- rappels sur le fonctionnement des conteneurs
- discussions autour des standards dans le monde de la conteneurisation
- criticité des images utilisées en conteneurisation

> Un peu d'air frais pour pas faire du `docker run` en boucle :)

- [Containers](#containers)
- [I. Conteneurs GNU/Linux](#i-conteneurs-gnulinux)
  - [1. Namespaces](#1-namespaces)
  - [2. Cgroups](#2-cgroups)
  - [3. Capabilities](#3-capabilities)
  - [4. Récap](#4-récap)
- [II. Standards](#ii-standards)
  - [1. OCI](#1-oci)
    - [A. Runtime](#a-runtime)
    - [B. Image](#b-image)
    - [C. Distribution](#c-distribution)
  - [2. CNCF](#2-cncf)
- [III. Distribution d'images](#iii-distribution-dimages)
  - [1. Registre](#1-registre)
  - [2. Confiance](#2-confiance)
  - [3. Build d'images](#3-build-dimages)
  - [4. Signatures](#4-signatures)
  - [5. Scans](#5-scans)
- [IV. Conclusion](#iv-conclusion)

# I. Conteneurs GNU/Linux

Les conteneurs sous GNU/Linux (créés avec Docker, Podman, la plupart des runtimes) reposent sur 3 mécanismes du noyau Linux :

- ***namespaces*** : isolation de processus
- ***cgroups*** : impose à un processus des restrictions d'accès aux ressources
- ***capabilities*** : privilèges accordés à un processus

Grâce à ces trois outils, on peut lancer un processus qui est isolé du système et dont on maîtrise le comportement. C'est ce qu'on appelle traditionnellement un "conteneur".

![I'm afraid to ask](../images/containers_afraid_to_ask.jpg)

## 1. Namespaces

Les *namespaces* sont des espaces où vivent les processus. **L'utilisation de namespaces permet d'isoler certaines partie du système pour un ou plusieurs processus donné(s).**

➜ Les namespaces ont une structure arborescente :

- lorsque vous démarrez votre système, vous êtes dans les namespaces "principaux" ou "parents"
- si vous créez de nouveaux namespaces, étant dans le namespace parent, vous voyez tout
- mais les namespaces enfants ne voient pas ce qu'il y au-dessus d'eux
- on peut créer un processus dans un nouveau namespace donné avec l'appel système `unshare()` (la commande `unshare` permet de l'utiliser depuis la ligne de commande)

---

➜ **Il existe 7 types de namespaces** :

- **`mnt` pour "mount"**
  - isole l'arbre de montage
  - output différent avec `df -h` ou `mount`
- **`pid` pour "process ID"**
  - isole l'arbre de processus
  - output différent avec un `ps -ef`
- **`ipc` pour "inter-process communication"**
  - isole certains canaux de communication inter-processus
  - comme les bus D-Bus
- **`net` pour "network"**
  - isole la pile réseau
  - le nouveau processus n'a qu'une interface loopback (sauf configuré explicitement autrement)
  - output de `ip a` différent
- **`uts`**
  - isolation de la gestion des noms de domaine
  - output de `hostname` différent
- **`user`**
  - isolation de l'arbre utilisateur
  - pour que les utilisateurs des nouveaux namespaces puissent être visibles et exister dans le namespace parent, un système de mapping est mis en place
  - `root` dans un namespace enfant correspond à un utilisateur non-privilégié dans le namespace parent
- **`cgroup`**
  - arborescence de cgroups différentes
  - `/sys/fs/cgroup` différent

> Par exemple, si deux processus (disons un terminal `bash` et un serveur `nginx`) sont dans deux namespaces `net` différents, ils n'auront pas accès aux mêmes interfaces réseau.

➜ **A chaque conteneur créé avec un outil comme Podman ou Docker, sont aussi créés des namespaces pour qu'il évolue à l'intérieur.** Le processus qui s'exécute dans le conteneur n'a donc pas de visibilité sur les ressources de l'hôte.

> Note : par défaut, Docker n'utilise pas les *namespaces* de type *user*. Podman le fait. Ainsi, un conteneur lancé avec la configuration par défaut de Docker s'exécute avec l'utilisateur `root` réel de la machine.

## 2. Cgroups

**Le terme *cgroup* désigne un mécanisme kernel de labellisation de processus et restriction de ressources appliquées aux processus.**  

En clair, cela permet de rassembler les processus en groupes. Pour ensuite potentiellement appliquer des restrictions d'accès aux ressources de la machines comme :

- utilisation CPU
- utilisation RAM
- I/O disque
- utilisation réseau
- utilisation *namespaces*

➜ L'utilisation généralisée des *cgroups* au sein des systèmes GNU/Linux permet un plus grand contrôle sur les processus et une administration plus aisée.

D'autres fonctionnalités qui en découlent sont le monitoring des processus par *cgroups* ou la priorisation de l'accès au ressources de la machines.

> Cela permet par exemple une attribution plus fine de ressources qu'avec des mécanismes comme le *oom_score* (pour l'utilisation de la RAM) ou le *nice_score* (pour l'utilisation du CPU). La gestion des ressources est ainsi centralisée.

**Les cgroups possèdent une structures arborescentes, comme une arborescence de fichiers. Mais au lieu de ranger des fichiers, on range des processus.**

## 3. Capabilities

**Les *capabilities* sont des droits accordés à des processus. Ces droits sont des privilèges élevés.**

Si l'utilisateur est tout-puissant au sein des systèmes GNU/Linux c'est parce que tous les processus qu'il lance possèdent toutes les *capabilities* par défaut.

➜ Il existe à ce jour 41 capabilities différentes, qui permettent toutes d'attribuer un privilège particulier à un processus (consultables [ici](https://man7.org/linux/man-pages/man7/capabilities.7.html) ou avec un `man capabilities` sur un système Linux).

Certaines des plus notables :

- `CAP_DAC_OVERRIDE` : permet d'outrepasser/bypass les droits POSIX (`rwxrwxrwx` )
- `CAP_SYS_CHROOT` : permet d'utiliser `chroot`  et d'avoir le contrôle sur les namespaces de type `mount`
- `CAP_SYS_TIME` : permet de définir l'heure du système et l'heure hardware (RTC)
- `CAP_SYS_ADMIN` : GODMODE ACTIVATED. A peu de choses près, un processus qui a cette *capa* a toutes les autres
- `CAP_NET_BIND_SERVICE` : permet d'écouter sur un port en dessous de 1024
- `CAP_NET_RAW` : permet d'envoyer des paquets de type RAW (souvent le cas pour ICMP, ainsi la commande `ping` a cette capa par défaut)

---

➜ **La gestion des capabilities est complexe** (chaque processus a 5 sets différents de capabilities). Le set "Effective" contient les capabilities utilisables par un processus donné.  
C'est à ce set que l'on fait référence lorsque l'on regarde les capabilities utilisées par un processus.

➜ Lorsqu'un processus est lancé, il est toujours lancé par un autre processus. Ainsi, il est l'enfant du processus parent.  
**Lorsqu'il est créé, le processus enfant PEUT NE PAS hériter de toutes les capabilites, mais uniquement celles que son parent autorise.**

## 4. Récap

Dans cette partie, on a vu 3 mécanismes du noyau Linux : les *namespaces*, les *cgroups* et les *capabilities*. 

➜ **Ce que l'on appelle couramment un "conteneur", c'est :**

- **un processus**
- qui s'exécute dans des *namespaces* différents de l'hôte
- qu'on a placé dans un *cgroup* spécifique
- auquel on a attribué des *capabilities* spécifiques

![There is no container](../images/just_another_process.png)

# II. Standards

## 1. OCI

L'[OCI](https://github.com/opencontainers) est une initiative qui vise à proposer trois standards :

- [*runtime*](https://github.com/opencontainers/runtime-spec)
- [*image*](https://github.com/opencontainers/image-spec)
- [*distribution*](https://github.com/opencontainers/distribution-spec)

### A. Runtime

Le *runtime* (ou *environnement d'exécution* en fr) désigne dans ce contexte l'application qui a la charge de préparer l'environnement pour qu'un processus s'exécute dans un environnement isolé, ce qu'on appelle plus couramment un "conteneur".

Dit plus concrètement, dans le cas de conteneurs GNU/Linux, le *runtime* est l'application qui se charge de la création des *namespaces*, des *cgroups* et qui va exécuter le processus.

Le *runtime* est un programme qui s'exécute pendant un très court instant, juste ce qu'il faut pour créer l'environnement, lancer le processus demandé dedans, puis quitter.

Le *runtime* le plus utilisé aujourd'hui (par défaut dans la plupart des installs de Docker, Podman, Kubernetes) est *runc*.

> Il en existe d'autres comme *crun* (conteneurs GNU/Linux), *youki* (conteneurs GNU/Linux) ou encore *kata* (machines virtuelles). Tout ceux-ci sont OCI-compliant, à l'instar de *runc*.

**L'OCI définit de façon standard ce que doit savoir faire un *runtime* de conteneurisation et comment on doit pouvoir interagir avec lui.**  
Ainsi il est possible de changer à tout moment de *runtime*, s'il est OCI-compliant, pour un autre qui l'est aussi.

### B. Image

Le deuxième standard émis par l'OCI concerne les images. Concrètement, dans le vocabulaire de la conteneurisation, une image désigne deux choses :

- des metadatas
- une archive compressée qui contient un filesystem

Une fois décompressée, cette image peut être utilisée pour lancer un ou plusieurs conteneurs. Le filesystem contenu dans l'image est en *read-only* est n'est jamais altéré directement.

L'OCI définit un format standard pour les metadatas ainsi que pour l'archive qui l'accompagne.

Plusieurs outils permettent de générer des images qui sont OCI-compliant comme `docker build` ou `buildah`.

**La standardisation des images permet une interopérabilité totale** : on peut partager des images plus aisément, les maintenir à jour et partager des bonnes pratiques. Cela les rend indépendant de l'outil qui a été utilisé pour les construire, mais aussi de l'outil qui va exécuter les conteneurs.

Cela permet aussi l'émergence d'autres outils qui se basent sur ce format standard pour créer un écosystème complet autour des images. On peut par exemple penser à du scanner de vuln dans les images ([Trivy](https://github.com/aquasecurity/trivy)).

### C. Distribution

**Ce troisième standard définit un format d'API utilisé pour distribuer des images.**

Tous les registres les plus utilisés implémentent ce standard, ce qui permet à d'autres outils de s'y connecter de façon interchangeables :

- le Docker Hub
- quay.io
- un registre interne avec Portus, ou Harbor

Des outils comme `docker`, `podman` ou `buildah` interagissent avec ces différents registres sans soucis, car ils implémentent tous le même standard.

## 2. CNCF

La [CNCF](https://www.cncf.io) est une fondation hébergée par la Linux Foundation qui vise à promouvoir des outils autour du monde du Cloud.

Beaucoup de ces outils sont directement liés à la conteneurisation. Il est difficile de passer à côté de cet organisme quand on s'intéresse au sujet. Liste non-exhaustive des projets qui vont nous intéresser :

- [containerd](https://containerd.io/)
- kubernetes
- [Harbor](https://goharbor.io/)
- CRI-O
- [falco](https://falco.org/)

# III. Distribution d'images

**La distribution d'images au sein d'un parc informatique est devenu un point critique des infrastructures applicatives.**

En effet les images peuvent aujourd'hui être à la base de beaucoup d'environnements d'exécution :

- sur les machines des dévs pour dév localement
- sur les machines de builds (typiquement CI/CD) pour setup des environnements temporaires
- sur les machines de prod pour exécuter des applications dans des environnements sécurisés (isolation, restriction, gestion des droits) et redondés

**Ainsi la provenance et le contenu de ces images doit être contrôlé et maîtrisé.**

## 1. Registre

**Un registre est un dépôt d'images sur le réseau.**

La solution utilisée est l'implémentation par défaut du standard de l'OCI hébergé sur [le dépôt `distribution` sur Github](https://github.com/distribution/distribution).

> Un registre évolué comme [Harbor](https://goharbor.io/) utilise sous le capot le registre de Distribution, et y ajoute tout plein de machins :)

Les solutions comme Harbor, ou encore le registre Gitlab, sont capables de gérer l'accès aux images *via* des comptes et une gestion de permissions.

## 2. Confiance

**La notion de confiance est primordiale ici**, autant que lorsqu'on télécharge des paquets. Il s'agit d'une problématique identique : des téléchargements réguliers vers l'extérieur.

Les images utilisées doivent :

- soit être construites en interne
- soit provenir de source sûre

Certaines registre d'images public comme le [Docker Hub](https://hub.docker.com) propose des images de confiance.

Pour le Docker Hub elles sont hébergées dans le dépôt *library*, comme l'image [Debian](https://hub.docker.com/_/debian).  
On peut repérer que l'image est dans le dépôt *library* par la mention "Image Officielle" et la présence de l'underscore dans l'URL : `/_/`.

![Docker Hub Official Image](../images/docker_hub_official_image.png)

## 3. Build d'images

La construction d'images ne doit pas être prise à la légère. Il est bon de veiller à :

- la légèreté des images
  - possibilité d'utiliser des images allégées
  - voire des images from scratch
- la performance des images
  - attention à l'utilisation d'Alpine
- la réutilisation des images
  - mise à profit du système de layers
  - créer des images de base qui contiennent des configs spécifiques au parc
- la mise à jour **très régulière** des images

**Des outils de CI/CD sont idéaux pour maintenir à jour les images disponibles dans un registre interne.**

## 4. Signatures

Il est possible de signer les images publiées dans un registre et de vérifier automatiquement la signature lorsque l'on récupère une image.

> On parle ici d'outils comme [Notary](https://github.com/notaryproject/notary).

Cela permet de garantir cryptographiquement la provenance de l'image et renforcer drastiquement la sécurité autour de la distribution des images.

Il devient extrêmement complexe d'usurper la distribution d'images afin de distribuer des images malveillantes.

## 5. Scans

A ajouter dans la chaîne de distribution, il est possible d'effectuer des scans de de sécurité avec des outils adaptés au monde de la conteneurisation. 

On parle ici de scan de vulnérabilités statiques (comme avec [Trivy](https://github.com/aquasecurity/trivy)) aussi bien que d'analyse dynamique pendant le fonctionnement d'un conteneur (comme avec [Falco](https://falco.org/)).

# IV. Conclusion

**Un conteneur est un simple processus.** Il est isolé du reste du système grâce à des technologies natives du noyau Linux.

**Le fait d'utiliser la conteneurisation revient à un moyen standard d'exécuter les applications.**

Un écosystème complet a vu le jour rapidement autour de la conteneurisation, du fait de sa large adoption. **La standardisation autour de ces outils est devenu un besoin urgent.**

**Des initiatives comme l'OCI ou la CNCF permettent de garder cet écosystème cohérent et libre.**

**La distribution des images, leur contenu et leur provenance est devenu un point extrêmenet critique** des infrastructures applicatives modernes. Fete gaf lé kopins.

---

[**Les TPs associés sont dispos ici.**](./tp-containers/README.md)

![Everyone get a container](../images/everyone_get_container.jpg)